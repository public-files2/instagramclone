import { Container } from "react-bootstrap"
import Feeds from "./feeds";
import PeopleStatus from "./peopleStatus";
import usePostHook from "./hooks";
import { Header } from "@/layout";
import AddPost from "./addPost";

const HomePage = () => {
  const {
    handleAddData,
    handleDelete,
    data,
    handleChange,
    addImgModalShow,
    setAddImgModalShow,
    closeImgModal,
    form,
    handleDrop,
    handleLikes,
    handleRemoveLikes,
    handleComments,
    handleDeleteComment,
  } = usePostHook()
  return (
    <Container className="transparentBg pb-md-5 border">
      <Header setAddImgModalShow={setAddImgModalShow} />
      <PeopleStatus />
      {
        data?.data?.map((item: any, index: any) => {
          return (
            <div key={index}>
              <Feeds
                form={form}
                handleChange={handleChange}
                handleComments={handleComments}
                items={item}
                handleLikes={handleLikes}
                handleRemoveLikes={handleRemoveLikes}
                handleDeleteComment={handleDeleteComment}
                handleDelete={handleDelete}
              />
            </div>
          )
        })
      }
      <AddPost
        handleAddData={handleAddData}
        form={form}
        handleChange={handleChange}
        addImgModalShow={addImgModalShow}
        closeImgModal={closeImgModal}
        handleDrop={handleDrop}
      />
    </Container>
  )
}
export default HomePage