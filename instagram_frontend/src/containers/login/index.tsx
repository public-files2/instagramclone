import useLoginHook from "./hooks"
import SignUp from "./signUp"
import Link from "next/link"
import styles from "./styles.module.css"
import { Col, Container, Row } from "react-bootstrap"
import { CustomButton, Text, TextField } from "@/conponents"

const Login = (props: any) => {

  const {
    screenType,
    setScreenType,
    handlechange,
    handleSignUp,
    onLogin,
    form,
  } = useLoginHook()
  return (
    <Container fluid style={{ height: '100vh' }} className="d-center">
      {
        screenType === "login" &&
        <Row className={`${styles.wrapper}`}>
          <Col xs={12} className="mb-4">
            <Text variant='h1' size={50}>Instagram</Text>
          </Col>
          <Col md={12}>
            <TextField onChange={handlechange} name="userName" value={form?.userName && form?.userName} type="text" placeholder="userName" required />
          </Col >
          <Col md={12}>
            <TextField onChange={handlechange} name="password" value={form?.password && form?.password} type="text" placeholder="Password" required />
          </Col>
          <Col md={12} className={`my-2`}>
            <CustomButton fullWidth={true} label="Login" onClick={() => onLogin()} className={`text-bold`} />
          </Col>
          <Col md={12} className={`my-2 d-center`}>
            <Link href="#">
              <Text>Forgot password?</Text>
            </Link>
          </Col>
          <div className="text-center">
            <div className={`${styles.register_link} d-center gap-1`} onClick={() => setScreenType('signup')}>
              {`Dont't have an account?`}
              <span>Sign Up</span>
            </div>
          </div>
        </Row>
      }
      {
        screenType == "signup" &&
        <SignUp
          handleSignUp={handleSignUp}
          handlechange={handlechange}
          form={form}
          screenType={screenType}
          setScreenType={setScreenType}
        />
      }
    </Container>
  )
}
export default Login