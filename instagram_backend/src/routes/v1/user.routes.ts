import { Router } from 'express';
import {
   deleteById,
   getAll,
   getById,
   loginUser,
   registerUser,
   updateById,
} from '../../controllers/users.controller';
const router = Router();

// GET /Users/
router.get('/', getAll);

// GET /Users/:id
router.get('/:id', getById);

// POST / registerUsers
router.post('/', registerUser);

// POST /loginUsers
router.post('/login', loginUser);

// PATCH /Users/:id
router.patch('/:id', updateById);

router.delete('/:id', deleteById);

export { router as usersRoute };
