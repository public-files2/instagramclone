import { useEffect, useState } from "react";
import styles from "./styles.module.css"
import { colors } from "@/constant";
const Text = (props: any) => {
  const {
    children,
    onClick,
    onMouseOver,
    className,
    textTransform = "",
    size = 14,
    color = colors?.black,
    lineHeight = 1,
    fontWeight = 500,
    fontFamily,
    variant = "span",
    style,
  } = props
  const [fontSize, setFontSize] = useState(size);
  const getSizeForMobileAndWeb = () => {
    const webSize = size;
    const mobileSize = 0.80 * size;
    const tabSize = 0.70 * size;
    return {
      webSize: webSize,
      tabSize: tabSize,
      mobileSize: mobileSize,
    };
  };

  const handleResize = () => {
    const windowWidth = window.innerWidth;
    const newSize =
      windowWidth < 768
        ? getSizeForMobileAndWeb().mobileSize
        : windowWidth < 968
          ? getSizeForMobileAndWeb().tabSize
          : getSizeForMobileAndWeb().webSize
    setFontSize(newSize);
  };

  useEffect(() => {
    window.addEventListener("resize", handleResize);
    handleResize();
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [size, fontSize]);
  const textStyle: any = {
    ...style,
    fontSize: fontSize,
    lineHeight: lineHeight,
    letterSpacing: ".8px",
    textTransform: textTransform,
    color: color,
    fontWeight: fontWeight,
    fontFamily: fontFamily,
    marginBottom: 0,
  };

  const variantStyles: { [key: string]: any } = {
    h1: { fontSize: fontSize, marginBottom: 0 },
    h2: { fontSize: fontSize, marginBottom: 0 },
    h3: { fontSize: fontSize, marginBottom: 0 },
    h4: { fontSize: fontSize, marginBottom: 0 },
    h5: { fontSize: fontSize, marginBottom: 0 },
    h6: { fontSize: fontSize, marginBottom: 0 },
  };

  const Element = variant;
  const elementStyle = variantStyles[variant] || {};

  return (
    <Element
      style={{ ...textStyle, ...elementStyle }}
      onMouseOver={onMouseOver}
      onClick={onClick}
      className={className}
    >
      {children}
    </Element>
  );
};

export default Text;
