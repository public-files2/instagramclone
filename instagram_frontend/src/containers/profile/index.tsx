import * as React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import Details from './details';
import EditProfileModal from './details/edit-profile-modal';
import { Text } from '@/conponents';
import useProfile from './hooks';

const Profile = () => {
  const {
    profileData,
    setProfileData,
    handleSubmit,
    handleDrop,
    handleRemove,
    openEditProfile,
    handleCloseEditProfile,
    handleOpenEditProfile,
    loggedInUser,
    setLoggedInUser,
  } = useProfile();

  return (
    <div>
      <Container>
        <Row className='justify-content-center mt-4'>
          <Col md={3}>
            <div className='border rounded p-2'>
              <div className='text-center'>
                <Text
                  variant="h3"
                  fontWeight={800}
                >
                  My Profile
                </Text>
                <Text
                  variant="body2"
                  fontWeight={700}
                >
                  Welcome Back!👋
                </Text>
              </div>
              <div className="mb-4">
                <Details
                  user={loggedInUser}
                  handleOpenEditProfile={
                    handleOpenEditProfile
                  }
                />
              </div>
            </div>
          </Col>
        </Row>
      </Container>
      <EditProfileModal
        openEditProfile={openEditProfile}
        handleCloseEditProfile={handleCloseEditProfile}
        profileData={profileData}
        handleSubmit={handleSubmit}
        setProfileData={setProfileData}
        setLoggedInUser={setLoggedInUser}
        handleDrop={handleDrop}
        handleRemove={handleRemove}
      />
    </div>
  );
};

export default Profile;
