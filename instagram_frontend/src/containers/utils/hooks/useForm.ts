import { useCallback, useState } from 'react';

export function useForm() {
	const [initialState] = useState('')
	const [form, setForm] = useState<any>(initialState);

	const handleChange = (event: any) => {
		let title = event.target.name
		let value = event.target.value
		setForm({ ...form, [title]: value })
	}

	const resetForm = useCallback(() => {
		// if (!_.isEqual(initialState, form)) {
		setForm(initialState);
		// }
	}, [form, initialState]);

	const setInForm = useCallback((name: any, value: any) => {
		// setForm((_form: any) => _.setIn(_form, name, value));
	}, []);

	// const handleSubmit = useCallback(
	// 	(event: any) => {
	// 		if (event) {
	// 			event.preventDefault();
	// 		}
	// 		if (onSubmit) {
	// 			onSubmit();
	// 		}
	// 	},
	// 	[onSubmit]
	// );

	return {
		form,
		handleChange,
		// handleSubmit,
		resetForm,
		setForm,
		setInForm
	};
}
