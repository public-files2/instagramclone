import { Request, Response, NextFunction } from 'express';
import PostService from '../services/post.services';

class PostController {
   static async getAllPost(req: Request, res: Response, next: NextFunction) {
      try {
         const { fileType, userId } = req.query;
         const post = await PostService.getAllPost(
            fileType,
            userId
         );
         return res.status(200).json(post);
      } catch (error) {
         next(error);
      }
   }

   static async getPostById(req: Request, res: Response, next: NextFunction) {
      try {
         const { id } = req.params;
         const post = await PostService.getPostById(id);
         return res.status(200).json(post);
      } catch (error) {
         next(error);
      }
   }

   static async createPost(req: Request, res: Response, next: NextFunction) {
      try {
         const { fileType, category, isPopular, title, url, userId } = req.body;
         const postData = {
            title: title,
            fileType: fileType,
            category: category,
            url: url,
            isPopular: isPopular,
            userId: userId,
         };
         const newPost = await PostService.createPost(postData);
         return res.status(201).json(newPost);
      } catch (error) {
         next(error);
      }
   }

   static async updatePost(req: Request, res: Response, next: NextFunction) {
      try {
         const { category, title, url, active, isPopular } = req.body;
         const postData = {
            title: title,
            url: url,
            active: active,
            isPopular: isPopular,
         };
         const updatedPost = await PostService.updatePost(req.params.id, postData);
         return res.status(200).json(updatedPost);
      } catch (error) {
         next(error);
      }
   }

   static async deletePost(req: Request, res: Response, next: NextFunction) {
      try {
         await PostService.deletePost(req.params.id);
         return res.sendStatus(204);
      } catch (error) {
         next(error);
      }
   }

   static async likePost(req: Request, res: Response, next: NextFunction) {
      try {
         const { id } = req.params;
         const userId: any = req.body.userId;

         const updatedPost = await PostService.likePost(id, userId);
         return res.status(200).json(updatedPost);
      } catch (error) {
         next(error);
      }
   }

   static async unlikePost(req: Request, res: Response, next: NextFunction) {
      try {
         const { id } = req.params;
         const userId = req.body.userId;

         const updatedPost = await PostService.unlikePost(id, userId);
         return res.status(200).json(updatedPost);
      } catch (error) {
         next(error);
      }
   }

   static async addComment(req: Request, res: Response, next: NextFunction) {
      try {
         const { id } = req.params;
         const { text, userId } = req.body;
         const comment = { userId, text };
         const updatedPost = await PostService.addComment(id, comment);
         return res.status(200).json(updatedPost);
      } catch (error) {
         next(error);
      }
   }

   static async deleteComment(req: Request, res: Response, next: NextFunction) {
      try {
         const { postId, commentId } = req.params;
         const updatedPost = await PostService.deleteComment(postId, commentId);
         return res.status(200).json(updatedPost);
      } catch (error) {
         next(error);
      }
   }
}

export default PostController;
