import { Router } from 'express';
import {
  deleteMessageController,
  createMessageController,
  getAllMessagesController,
  updateMessageController,
} from '../../controllers/message.contoller'

const router = Router();

// POST /messages
router.post('/', createMessageController);

// GET /messages
router.get('/', getAllMessagesController);

// PATCH /messages/:id
router.patch('/:id', updateMessageController);

// DELETE /messages/:id
router.delete('/:id', deleteMessageController);

export { router as messagesRoute };
