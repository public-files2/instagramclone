import { Col, Row } from "react-bootstrap"
import styles from "../styles.module.css"
import { CustomButton, Text, TextField } from "@/conponents"
import Link from "next/link"
const SignUp = (props: any) => {
  const {
    setScreenType,
    handlechange,
    form,
    handleSignUp,
  } = props
  return (
    <>
      <div className={`${styles.wrapper}`}>
        <Row action="">
          <Col xs={12} className="mb-4 text-center">
            <Text variant='h1' size={50}>Sign Up</Text>
            <div className='mt-3 px-2'>

              <Text >Sign up to see photos and videos from your friends.</Text>
            </div>
          </Col>
          <Col md={6} >
            <TextField onChange={handlechange} name="firstName" value={form?.firstName && form?.firstName} type="text" placeholder="First Name" required />
          </Col>
          <Col md={6} >
            <TextField onChange={handlechange} name="lastName" value={form?.lastName && form?.lastName} type="text" placeholder="Last Name" required />
          </Col>
          <Col md={12} >
            <TextField onChange={handlechange} name="email" value={form?.email && form?.email} type="text" placeholder="Email" required />
          </Col>
          <Col md={12} >
            <TextField onChange={handlechange} name="userName" value={form?.userName && form?.userName} type="text" placeholder="userName" required />
          </Col>
          <Col >
            <TextField onChange={handlechange} name="password" value={form?.password && form?.password} type="text" placeholder="Password" required />
          </Col>
          <div className="text-center mt-2">
            <div className="text-center mt-2">
              <Text size={10}>People who use our service may have uploaded your contact information to Instagram.<Link href=""> Learn more</Link></Text>
            </div>
            <div className="text-center mt-2">
              <Text size={10}>By signing up, you agree to our Terms, <Link href="">Privacy Policy and Cookies Policy.</Link></Text>
            </div>
            <CustomButton fullWidth={true} label="Sign Up" onClick={() => { handleSignUp() }} />
            <div role='button' className={`${styles.register_link} d-center gap-1`} onClick={(e) => setScreenType('login')}>
              Already have an account?
              <span>Login</span>
            </div>
          </div>
        </Row>
      </div>
    </>
  )
}
export default SignUp