import { Document, model, Schema } from 'mongoose';

export interface IMessage extends Document {
   receiver: string,
   sender: string;
   room: string,
   message: string,
   sent: boolean,
   createdtime: Date,
}

const messageSchema = new Schema<IMessage>(
   {
      receiver: { type: String, },
      sender: { type: String },
      room: { type: String },
      message: { type: String },
      sent: { type: Boolean, default: false },
      createdtime: { type: Date, default: Date.now },
   },
);

export default model<IMessage>('Message', messageSchema);
