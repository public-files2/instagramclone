import { AuthContext } from "@/context/user";
import { useContext } from "react";

export function convertCamelCase(inputString: any) {
  if (!inputString) {
    return '';
  }
  const camelCaseString = inputString
    .split(' ')
    .map((word: any, index: any) => (index === 0 ? word.toLowerCase() : word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()))
    .join('');

  return camelCaseString
}

export const UserAuthorization = (id: any) => {
  const { loggedInUser } = useContext(AuthContext);
  if (id == loggedInUser?._id) {
    return true
  } else {
    return false
  }
}