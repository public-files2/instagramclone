import { CustomButton, Text } from '@/conponents';
import React from 'react';

const VideoCallComponent = ({ startVideoCall, endVideoCall, localVideoRef, remoteVideoRef }: any) => {

  return (
    <div>
      <video className='rounded' height='60px' ref={localVideoRef} autoPlay muted />
      <video className='rounded' height='60px' ref={remoteVideoRef} autoPlay />
      <div className='d-center gap-2'>
        <CustomButton
          sizeVariant="CUSTOM"
          label={<Text size={10}>Start Video Call</Text>}
          onClick={startVideoCall}
        />
        <CustomButton
          sizeVariant="CUSTOM"
          label={<Text size={10}>End Video Call</Text>}
          onClick={endVideoCall}
        />

      </div>
    </div>
  );
};

export default VideoCallComponent;
