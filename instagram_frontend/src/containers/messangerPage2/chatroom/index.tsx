// components/ChatArea.tsx
import React, { useContext, useEffect, useRef, useState } from 'react';
import styles from './style.module.css';
import { Col, Row } from 'react-bootstrap';
import { CustomButton, StatusUI, Text, TextField } from '@/conponents';
import { AuthContext } from '@/context/user';
import { BiVideo } from 'react-icons/bi';
import VideoCallComponent from '../videoCall';
import SimplePeer from 'simple-peer';

const ChatRoom = ({ socket, setIsVideoCallActive, isVideoCallActive, selectedUser, messages, setMessages }: any) => {
  const [message, setMessage] = useState('');
  const { loggedInUser } = useContext(AuthContext);

  useEffect(() => {
    socket.on('receiver_message', (data: any) => {
      const { room, message, username, sender, receiver, createdtime } = data;
      setMessages((state: any) => [
        ...state,
        {
          message: message,
          username: username,
          createdtime: createdtime,
          receiver: receiver,
          room: room,
          sender: sender,
        },
      ]);
    });

    return () => socket.off('receiver_message');
  }, [socket]);

  function formatDateFromTimestamp(timestamp: any) {
    const date = new Date(timestamp);
    return date.toLocaleString();
  }

  const sendMessage = () => {
    if (message !== '') {
      const sendingBy = {
        room: selectedUser?.room,
        username: loggedInUser?.firstName,
        message: message,
        sender: loggedInUser?._id,
        receiver: selectedUser?.room,
        createdtime: Date.now(),
        sent: true,
      }
      setMessages((prevMessages: any) => [...prevMessages, sendingBy]);
      socket.emit('send_message', sendingBy);
      // setMessage('');
    }
  };

  const messagesColumnRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const scrollToBottom = () => {
      if (messagesColumnRef.current) {
        messagesColumnRef.current.scrollTop = messagesColumnRef.current.scrollHeight;
      }
    };

    scrollToBottom();
  }, [messages]);

  // vedio call setup
  const [peer, setPeer] = useState<any>(null);
  const [stream, setStream] = useState<any>(null);
  const localVideoRef = useRef<any>(null);
  const remoteVideoRef = useRef<any>(null);
  const [isVideoCallNotification, setVideoCallNotification] = useState(false);
  useEffect(() => {
    if (socket) {
      socket.on('offer', (data: any) => {
        const newPeer = new SimplePeer({ initiator: false, trickle: false });
        setPeer(newPeer);

        newPeer.on('signal', (answer: any) => {
          socket.emit('answer', answer);
        });

        newPeer.signal(data?.offer);

        newPeer.on('stream', (remoteStream: any) => {
          remoteVideoRef.current.srcObject = remoteStream;
        });
      });

      socket.on('answer', (answer: any) => {
        if (peer) {
          peer.signal(answer);
        }
      });

      socket.on('ice-candidate', (candidate: any) => {
        if (peer) {
          peer.addIceCandidate(candidate);
        }
      });
    }
  }, [socket, peer]);

  const startVideoCall = async () => {
    setIsVideoCallActive(true)
    // try {
    const userMediaStream = await navigator.mediaDevices.getUserMedia({ video: true, audio: true });
    setStream(userMediaStream);
    // ... rest of the code

    localVideoRef.current.srcObject = userMediaStream;

    const newPeer = new SimplePeer({ initiator: true, stream: userMediaStream, trickle: false });
    setPeer(newPeer);


    newPeer.on('signal', (offer: any) => {
      const sendingBy = {
        room: selectedUser?.room,
        offer: offer,
        sender: loggedInUser?._id,
        receiver: selectedUser?.room,
        createdtime: Date.now(),
      }
      socket.emit('offer', sendingBy);
    });

    newPeer.on('stream', (remoteStream: any) => {
      remoteVideoRef.current.srcObject = remoteStream;
    });

    newPeer.on('ice-candidate', (candidate: any) => {
      socket.emit('ice-candidate', candidate);
    });
    // } catch (error) {
    //   // Handle permission denied or other errors
    //   console.error('Error accessing media devices:', error);
    // }
  };

  const endVideoCall = () => {
    setVideoCallNotification(false);
    setIsVideoCallActive(false)
    if (peer) {
      peer.destroy();
      setPeer(null);
    }
    if (stream) {
      stream.getTracks().forEach((track: any) => track.stop());
      setStream(null);
    }
  };

  useEffect(() => {
    socket.on('video_call_notification', (data: any) => {
      console.log('Received video call notification:', data);
      setIsVideoCallActive(true);
      setVideoCallNotification(true);
    });

    return () => socket.off('video_call_notification');
  }, [socket]);

  console.log(selectedUser, 'selectedUser', loggedInUser?._id)
  const filterMessage = () => {
    // let data = messages?.filter((value: any) => value?.receiver == loggedInUser?._id && value?.sender == selectedUser?._id)
    // let data = messages
    let data = messages?.filter((value: any) => value?.receiver == loggedInUser?._id)
    return data
  }
  console.log(filterMessage(), 'filterMessagefilterMessagessage')
  return (
    <>
      <div className='d-center gap-2'>
        <Text size={10}>room: {selectedUser?.room}</Text>
        <CustomButton
          sizeVariant="CUSTOM"
          label={<BiVideo />}
          onClick={() => { startVideoCall() }}
        />
      </div>
      <Row>
        <Col md={12} className='border'>
          <div className="d-flex align-items-center gap-1 my-1">
            <StatusUI src={selectedUser?.profileImg ? selectedUser?.profileImg : "/assets/camera.webp"} width={25} height={25} />
            <div className="d-flex flex-column">
              <Text size={11} textTransform="">
                {selectedUser?.username}
              </Text>
            </div>
          </div>
        </Col>
        <Col md={12} className='border'>
          <div className={styles.messagesColumn} ref={messagesColumnRef}>
            {isVideoCallActive ?
              <VideoCallComponent
                socket={socket}
                selectedUser={selectedUser}
                startVideoCall={startVideoCall}
                endVideoCall={endVideoCall}
                peer={peer}
                stream={stream}
                localVideoRef={localVideoRef}
                remoteVideoRef={remoteVideoRef}
              />
              :
              <>{
                messages?.length > 0 ?
                  <>{messages?.map((item: any, i: any) => (
                    <div className={`d-flex ${item?.sender == loggedInUser?._id ? "justify-content-end" : "justify-content-start"}`} key={i}>
                      <div className={`${styles.message}`} >
                        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                          <Text color="#fff" size={12}>{item?.username}</Text>
                          <Text color="#fff" size={12}>
                            {item?.sender == loggedInUser?._id ? 'Sent at' : 'Received at'}
                            {formatDateFromTimestamp(item?.createdtime)}
                          </Text>
                        </div>
                        <Text color="#fff" size={12}>Message: {item?.message}</Text>
                        <br />
                      </div>
                    </div>
                  ))}</>
                  :
                  <div className='d-center'>
                    <Text size={12}>Send hi to start conversation...</Text>
                  </div>
              }</>}
          </div>
        </Col>
        <Col md={12}>
          {isVideoCallNotification && (
            <div className="video-call-notification">
              <Text size={16}>Incoming Video Call</Text>
              <div className="d-center gap-2">
                <CustomButton
                  sizeVariant="CUSTOM"
                  label={<Text size={10}>Accept Video Call</Text>}
                />
                <CustomButton sizeVariant="CUSTOM" label="Reject" onClick={() => endVideoCall()} />
              </div>
            </div>
          )}
        </Col>
        <Col md={12} className='border'>
          <div className={styles.sendMessageContainer}>
            <TextField
              className={styles.messageInput}
              placeholder='Message...'
              onChange={(e: any) => setMessage(e.target.value)}
              value={message}
            />
            <button className='btn btn-primary' onClick={sendMessage}>
              Send Message
            </button>
          </div>
        </Col>
      </Row>
    </>
  );
};

export default ChatRoom;