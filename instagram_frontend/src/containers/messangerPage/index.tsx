import React, { useState, useEffect, ChangeEvent, useContext } from 'react';
import ChatArea from './chatArea';
import { AuthContext } from '@/context/user';

const WebSocketComponent: React.FC = () => {
  const { loggedInUser, setLoggedInUser } = useContext(AuthContext);

  const [messages, setMessages] = useState<any>([]);
  const [input, setInput] = useState<string>('');
  const [socket, setSocket] = useState<any>(null);

  useEffect(() => {
    const client: WebSocket = new WebSocket('ws://localhost:5000'); // WebSocket server URL

    client.onopen = () => {
      console.log('WebSocket Client Connected');
      setSocket(client);
    };

    client.onmessage = (event: any) => {
      const newMessages: string[] = [...messages, event.data];
      setMessages(newMessages);
    };

    client.onclose = () => {
      console.log('WebSocket Client Disconnected');
    };

    return () => {
      client.close();
    };
  }, [messages]);

  const sendMessage = () => {
    const data: any = {
      content: input.trim() !== '' && input,
      clientOffset: Date.now(),
      sender: loggedInUser?.firstName
    }
    if (socket && input.trim() !== '') {
      socket.send(JSON.stringify(data));
      setMessages([...messages, input]);
      // setInput('');
    }
  };

  return (
    <div>
      <h1>WebSocket Chat</h1>
      <ChatArea messages={messages} setMessages={setMessages} sendMessage={sendMessage} input={input} setInput={setInput} />
    </div>
  );
};

export default WebSocketComponent;
