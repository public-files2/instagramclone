// components/StarField.js
import { useEffect, useState } from 'react';
import styles from './404.module.css';

const StarField = () => {
  const [stars, setStars] = useState<any>([]);

  useEffect(() => {
    const createStar = () => {
      const newStar: any = {
        right: Math.random() * 500,
        top: Math.random() * window.innerHeight,
        id: Date.now(),
      };
      setStars((prevStars: any) => [...prevStars, newStar]);
    };

    const runStars = () => {
      setStars((prevStars: any) =>
        prevStars.map((star: any) => ({
          ...star,
          right: star.right + 3,
        }))
      );
    };

    const removeOffScreenStars = () => {
      setStars((prevStars: any) => prevStars.filter((star: any) => star.right < window.innerWidth));
    };

    const intervalId = setInterval(() => {
      createStar();
    }, 100);

    const runStarsIntervalId = setInterval(() => {
      runStars();
      removeOffScreenStars();
    }, 10);

    return () => {
      clearInterval(intervalId);
      clearInterval(runStarsIntervalId);
    };
  }, []);

  return (
    <>
      {stars.map((star: any) => (
        <div key={star.id} className={styles.star} style={{ top: `${star.top}px`, right: `${star.right}px` }} />
      ))}
    </>
  );
};

export default StarField;
