import express from 'express';
import { usersRoute } from './user.routes';
import { uploadFileRoute } from './uploder.routes';
import { postRoute } from './post.routes';
import { messagesRoute } from './message.routes';

const routes = express.Router();

// Routes
routes.use('/v1/users', usersRoute);
routes.use('/v1/fileuploader', uploadFileRoute);
routes.use('/v1/posts', postRoute);
routes.use('/v1/messages', messagesRoute);

export default routes;
