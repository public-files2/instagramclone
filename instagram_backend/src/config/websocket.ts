import messageModel from '../models/message.model';
import WebSocket from 'ws';

const websocketConnect = (server: any) => {
  const wss = new WebSocket.Server({ server });

  const clients = new Map();
  wss.on('connection', async (ws) => {
    console.log('WebSocket Client Connected');

    // Assign a unique identifier to each client
    const clientId = Math.random().toString(36).substring(7);
    clients.set(clientId, ws);

    ws.on('message', async (message: any) => {
      console.log(`Received: ${message}`);

      // Save the message to MongoDB
      try {
        const { content, clientOffset, sender } = JSON.parse(message);

        const savedMessage = await messageModel.create({ content: content, sender: sender, clientOffset: clientOffset, });
        // if (/* your condition here */) {
        // ws.send(JSON.stringify({ content: savedMessage.content, sender: savedMessage._id }));
        // } else {
        wss.clients.forEach((client) => {
          if (client !== ws && client.readyState === WebSocket.OPEN) {
            client.send(JSON.stringify({ content: savedMessage?.message, sender: savedMessage.sender }));
          }
        });
        // }
      } catch (error) {
        console.error('Error saving message:', error);
      }
    });

    ws.on('close', () => {
      console.log('WebSocket Client Disconnected');
    });
  });
}
export default websocketConnect