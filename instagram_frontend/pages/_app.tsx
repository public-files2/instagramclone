import { AuthContext } from '@/context/user';
import '@/styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import type { AppProps } from 'next/app'
import { Router, useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { ToastContainer } from 'react-toastify';

export default function App({ Component, pageProps }: AppProps) {
  Router.events.on("routeChangeStart", (url) => {
    console.log("route is changing", url)
  })
  const [loggedInUser, setLoggedInUser] = useState<any>(null);
  const [queryClient] = useState(() => new QueryClient());
  const router = useRouter()
  useEffect(() => {
    const loggedUser: any = localStorage.getItem('LOGGED_USER');
    const fetchData = async () => {
      try {
        if (loggedUser) {
          const user = JSON.parse(loggedUser);
          setLoggedInUser(user);
        } else {
          router.push('/login')
          localStorage.removeItem('LOGGED_USER');
          setLoggedInUser(null);
        }
      } catch (error) {
        console.error('Error in fetching user data:', error);
      }
    };

    fetchData();
  }, []);
  return (
    <div className='globalContentColors'>
      <AuthContext.Provider value={{ loggedInUser, setLoggedInUser }}>
        <QueryClientProvider client={queryClient}>
          <ToastContainer position="top-right"
            autoClose={1000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="light" />
          <Component {...pageProps} />
        </QueryClientProvider>
      </AuthContext.Provider>
    </div>
  )
}
