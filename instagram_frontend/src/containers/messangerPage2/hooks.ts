import { useContext, useEffect, useState } from "react";
import { AuthContext } from "@/context/user";
import { KEYS } from "@/api/handlers";
import { getAllUsers } from "@/api/services/user";
import { useQuery } from "@tanstack/react-query";
import { getMessages } from "@/api/services/messanger";

const useMessanger = () => {
   const { loggedInUser, setLoggedInUser } = useContext(AuthContext);
   const [selectedUser, setSelectedUser] = useState<any>();

   useEffect(() => {
   }, [loggedInUser?._id]);

   const { data: allUsers, refetch } = useQuery({
      queryKey: [KEYS.user],
      queryFn: getAllUsers,
   });
   // queryKey: [KEYS.message, selectedUser?.room, loggedInUser?._id, selectedUser?._id],

   const { data: allMessages, isFetching } = useQuery({
      queryKey: [KEYS.message, '', '', ''],
      queryFn: getMessages,
      enabled: !!selectedUser?.room
   });

   return {
      isFetching,
      allUsers,
      selectedUser,
      setSelectedUser,
      allMessages
   };
};

export default useMessanger;
