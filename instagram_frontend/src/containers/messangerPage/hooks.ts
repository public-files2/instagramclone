import { useContext, useEffect, useState } from "react";
import useFileUpload from "../utils/hooks/useFileUpload";
import { useMutation } from "@tanstack/react-query";
import { update_user } from "@/api/services/user";
import { toast } from "react-toastify";
import { validateEmail } from "../utils/form-validators";
import { AuthContext } from "@/context/user";
import { updateLoggedInUser } from "../utils/functions/update-user";

const useProfile = () => {
   const { loggedInUser, setLoggedInUser } = useContext(AuthContext);
   const { imageUrl, callImageUpload } = useFileUpload();
   const [openEditProfile, setOpenEditProfile] = useState(false);
   const handleOpenEditProfile = () => setOpenEditProfile(true);
   const handleCloseEditProfile = () => setOpenEditProfile(false);
   const [profileData, setProfileData] = useState<any>();
   useEffect(() => {
      if (imageUrl) {
         setProfileData({ ...profileData, profileImg: imageUrl });
      }
   }, [imageUrl]);

   useEffect(() => {
      setProfileData({ ...loggedInUser });
   }, [profileData?._id]);

   const updateMutation = useMutation({
      mutationFn: (data: any) => update_user(data, loggedInUser?._id),
      onError: (error) => {
         console.log(error, `rolling back optimistic update`);
      },
      onSuccess: (data: any,) => {
         if (data) {
            setLoggedInUser(data);
            updateLoggedInUser(loggedInUser, setLoggedInUser)
            toast.success('Detail Updated Successfully!');
            handleCloseEditProfile();
         }
      },
   });

   const handleSubmit = () => {
      if (validateEmail(profileData?.email) && profileData?.email?.length > 0) {
         updateMutation.mutate(profileData);
      } else {
         toast.error('Enter valid Email', { autoClose: 2000 });
      }
   };

   const handleDrop = (acceptedFiles: any) => {
      const file = acceptedFiles;
      if (file) {
         const formData = new FormData();
         formData.append('image', file);
         callImageUpload(formData, 'gallery');
      }
   };
   const handleRemove = () => {
      setProfileData(" ");
      setProfileData({ ...profileData, profileImg: imageUrl });
   }
   return {
      profileData,
      setProfileData,
      handleSubmit,
      updateMutation,
      handleDrop,
      handleRemove,
      openEditProfile,
      handleCloseEditProfile,
      handleOpenEditProfile,
      loggedInUser,
      setLoggedInUser,
   };
};

export default useProfile;
