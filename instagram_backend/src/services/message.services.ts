import Message, { IMessage } from '../models/message.model';

export const createMessage = async (data: any): Promise<IMessage> => {
   const message: IMessage = new Message(data);
   return await message.save();
};

export const getAllMessages = async (room: any, receiver: any, sender: any): Promise<IMessage[]> => {
   const query: any = {};
   if (room) {
      query.room = room;
   }
   if (receiver) {
      query.receiver = receiver;
   }
   if (sender) {
      query.sender = sender;
   }
   return await Message.find(query).exec();
};

export const getMessageByRoom = async (room: any): Promise<IMessage[] | null> => {
   if (!room) {
      return null;
   }
   return await Message.find({ 'room': room }).exec();
};

export const getMessageById = async (id: string): Promise<IMessage | null> => {
   return await Message.findById(id);
};

export const updateMessage = async (id: string, data: any): Promise<IMessage | null> => {
   const message = await Message.findById(id).exec();
   if (!message) {
      return null;
   }
   message.set(data);
   return await message.save();
};

export const deleteMessage = async (id: string): Promise<IMessage | any> => {
   await Message.findByIdAndDelete(id);
   return 'Message deleted';
};
