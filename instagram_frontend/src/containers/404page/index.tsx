import Head from 'next/head';
import styles from './404.module.css';
import StarField from './star';

const NotFoundPage = () => {
  return (
    <div className={styles.container}>
      <Head>
        <title>404 - Page Not Found</title>
      </Head>
      <div className={styles.text}>
        <div>ERROR</div>
        <h1>404</h1>
        <hr />
        <div>Page Not Found</div>
      </div>
      <div className={styles.astronaut}>
        <img
          src="https://images.vexels.com/media/users/3/152639/isolated/preview/506b575739e90613428cdb399175e2c8-space-astronaut-cartoon-by-vexels.png"
          alt=""
          className={styles.src}
        />
      </div>
      <StarField />
    </div>
  );
};

export default NotFoundPage;
