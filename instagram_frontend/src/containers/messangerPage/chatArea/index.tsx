// components/ChatArea.tsx
import React, { useState } from 'react';
import { Tab, Tabs, Form, Button } from 'react-bootstrap';
import './style.module.css';

const ChatArea = (props: any) => {
  const { messages, input, setInput, sendMessage } = props
  // console.log(messages, "messages")
  return (
    <div className="section-message">
      <Tabs defaultActiveKey="open" id="chat-tabs">
        <Tab eventKey="open" title="Open">
          <div className="chat-list">
            {messages.map((message: any, index: any) => {
              const jsonString = JSON.stringify(message)
              const parsedObject = JSON.parse(jsonString)
              return (
                <div key={index}>
                  <div>{parsedObject?.content}</div>
                  <div>{message?.content}</div>
                </div>
              )
            })
            }
          </div>
        </Tab>
        <Tab eventKey="closed" title="Closed">
          <div className="chat-list">
            {/* Render your list of closed chats here */}
          </div>
        </Tab>
      </Tabs>

      <div className="chat-box">
        <Form>
          <Form.Group className="mb-3">
            <Form.Control
              type="text"
              placeholder="Write message…"
              value={input}
              onChange={(e) => setInput(e.target.value)}
            />
          </Form.Group>
          <Button variant="primary" onClick={sendMessage}>
            Send
          </Button>
        </Form>
        <div>
          {messages.map((message: any, index: any) => {
            return (
              <div key={index}>
                <div>{message}</div>
              </div>
            )
          })
          }
        </div>
      </div>
    </div>
  );
};

export default ChatArea;
