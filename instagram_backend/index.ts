import express, { Application } from 'express';
import cors from 'cors';
import routes from './src/routes/v1/index';
import 'dotenv/config';
import bodyParser from 'body-parser';
import connectDB from './src/config/db';
import websocketConnect from './src/config/websocket';
import { createServer } from 'http';
import { Server as SocketIoServer } from 'socket.io';
import socketIoConnect from './src/config/socketIo';

connectDB();
const app: Application = express();
const server = createServer(app);
const io = new SocketIoServer(server, {
  cors: {
    origin: 'http://localhost:3000',
    methods: ['GET', 'POST'],
  },
});

// Middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors({ origin: '*' }));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// Routes
app.use('/', routes);
// websocketConnect(server)
socketIoConnect(io)

const port = process.env.PORT || 5002;

server.listen(port as number, '0.0.0.0', () => {
  console.log(`Server is started on port ${port}`);
});