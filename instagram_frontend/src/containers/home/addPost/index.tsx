import { CustomButton, Text, TextField } from "@/conponents"
import { Form, Modal } from "react-bootstrap"

const AddPost = (props: any) => {
  const {
    handleAddData,
    form,
    handleChange,
    addImgModalShow,
    closeImgModal,
    handleDrop,
  } = props
  const handleChangeImage = (event: any) => {
    handleDrop(event.target.files[0]);
  };

  const handleSelect = () => {
    document.getElementById('fileInput')?.click();
  }
  return (
    <div className="d-center" >
      <Modal
        show={addImgModalShow} onHide={() => closeImgModal()}
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            <Text>Add New Post</Text>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="text-center">
            <div onClick={() => handleSelect()} style={{ cursor: 'pointer' }}>
              <img src={form?.url ? form?.url : "/assets/camera.webp"} className="rounded-circle" width="170px" height="170px" />
              <div>
                <Text>Choose here</Text>
              </div>
            </div>
            <Form>
              <input
                type="file"
                id="fileInput"
                style={{ display: 'none' }}
                onChange={(e: any) => handleChangeImage(e)}
              />
            </Form>
          </div>
          <div className="text-center">
            <TextField className='border-0' onChange={handleChange} placeholder="description....." name='title' value={form?.title && form?.title} />
          </div>
        </Modal.Body>
        <Modal.Footer>
          <div className="p-2 d-center gap-1 justify-content-end" >
            <CustomButton sizeVariant="SMALL" label="Close" onClick={() => closeImgModal()} />
            <CustomButton
              sizeVariant="SMALL"
              label={"Post"} onClick={() => { handleAddData() }}
              disabled={form?.url ? false : true}
            />
          </div>
        </Modal.Footer>
      </Modal>
    </div>
  )
}
export default AddPost