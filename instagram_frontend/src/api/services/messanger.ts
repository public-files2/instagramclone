import { apiEndPoint } from '../handlers';
import api from '../handlers/api';
const getMessages = async ({ queryKey }: any) => {
   if (!Array.isArray(queryKey)) {
      throw new Error('queryKey must be an array');
   }
   const [_, room, receiver, sender] = queryKey;
   const params = {
      room: room,
      receiver: receiver,
      sender: sender
   };

   const { data } = await api.get(apiEndPoint.messages, {
      params: params,
   });
   return data;
};

export { getMessages };
