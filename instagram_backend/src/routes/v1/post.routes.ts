import PostController from '../../controllers/post.contoller';
import { Router } from 'express';

const router = Router();

// GET / - Retrieve all Post
router.get('/', PostController.getAllPost);

// GET /:id - Retrieve Post By id
router.get('/:id', PostController.getPostById);

// POST / - Create a new Post
router.post('/', PostController.createPost);

// PATCH /id - Update an existing Post
router.patch('/:id', PostController.updatePost);

// DELETE /id - Delete an existing Post
router.delete('/:id', PostController.deletePost);

// POST /:id/like - Like a post
router.post('/like/:id', PostController.likePost);

// POST /:id/unlike - Unlike a post
router.post('/unlike/:id', PostController.unlikePost);

// POST /:id/comment - Add a comment to a post
router.post('/comment/:id', PostController.addComment);

// DELETE /:id/comment/:commentId - Delete a comment from a post
router.delete('/comment/:postId/:commentId', PostController.deleteComment);

export { router as postRoute };
