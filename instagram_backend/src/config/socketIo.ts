import { createMessage, getAllMessages } from '../services/message.services';

const socketIoConnect = (io: any) => {
  let chatRoom = '';
  let targetUser = ''
  let allUsers: { id: any; username: any; room: any; }[] = [];
  io.on('connection', (socket: any) => {
    // console.log(`User connected ${socket.id}`);

    // Listen for 'send_message' event from the client
    socket.on('send_message', async (data: any) => {
      const { room, message, username, sender, receiver, createdtime } = data;
      console.log('Received message from client:', sender);
      // getAllMessages(sender)
      // Broadcast the receiverd message to all users in the room
      socket.to(sender).emit('receiver_message', {
        message: message,
        username: username,
        createdtime: createdtime,
        receiver: receiver,
        room: room,
        sender: sender,
      });
      await createMessage(data)
    });
    // Add a user to a room
    socket.on('join_room', (data: any) => {
      const { username, room, sender, receiver } = data;

      socket.join(room);
    });
    // ---------------------vedeo--------------------------------
    socket.on('offer', (data: any) => {
      const { offer, room, sender, receiver } = data;
      // Handle incoming offer and send it to the other peer
      socket.to(sender).emit('offer', data);

      // Notify the target user about the incoming video call
      io.to(room).emit('video_call_notification', {
        sender: sender,
        room: room,
        username: receiver,
      });
    });

    socket.on('video_call_notification', (data: any) => {
      console.log('Received video call notification:', data);

      // Emit the notification to the specific room
      io.to(data.room).emit('video_call_notification', data);
    });


    socket.on('answer', (data: any) => {
      socket.to(data.target).emit('answer', data.answer);
    });

    socket.on('ice-candidate', (data: any) => {
      // Handle incoming ICE candidate and send it to the other peer
      socket.to(data.target).emit('ice-candidate', data.candidate);
    });

    socket.on('disconnect', () => {
      console.log(`User disconnected ${socket.id}`);
      allUsers = allUsers.filter((user: any) => user.id !== socket.id);
      const disconnectedUser = allUsers.find((user: any) => user.id === socket.id);
      if (disconnectedUser) {
        createMessage({
          room: disconnectedUser.room,
          message: `${disconnectedUser.username} has left the chat room`,
          sender: disconnectedUser.username,
          receiver: '', // You may need to specify the receiver depending on your use case
          createdtime: Date.now(),
        });
      }
      // Send the updated user list to all users in the room
      io.to(chatRoom).emit('chatroom_users', allUsers);
    });
  });
}
export default socketIoConnect