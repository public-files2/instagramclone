import { NextFunction, Request, Response } from 'express';
import {
   createUser,
   deleteUser,
   getAllUsers,
   getUserById,
   getUserByIdV2,
   getUserByUsername,
   updateUser,
} from '../services/user.services';

export const registerUser = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const {
         firstName = '',
         lastName = '',
         email = '',
         mobile = '',
         profileImg = '',
         userName = '',
         password = '',
      } = req?.body;
      let existingUser

      if (!userName) {
         return res.status(400).json({ message: 'Username is required.', status: 'success' });
      } else {
         existingUser = await getUserByUsername(userName);
      }
      if (existingUser) {
         return res.status(400).json({ message: 'Username already exists' });
      }
      const data = {
         firstName: firstName,
         lastName: lastName,
         email: email,
         mobile: mobile,
         profileImg: profileImg,
         userName: userName,
         password: password,
      };
      const savedUser = await createUser(data);
      res.status(201).json({ msg: 'User Created', data: savedUser });

   } catch (error) {
      next(error);
   }
};

export const getAll = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const { type, city } = req.query;
      const users = await getAllUsers(type, city);
      res.json(users);
   } catch (error) {
      next(error);
   }
};

export const getById = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const users: any = await getUserById(req.params.id);
      if (!users) {
         return res.status(404).json({ message: 'Users not found' });
      }
      res.json({ ...users?._doc });
   } catch (error) {
      next(error);
   }
};

export const updateById = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const {
         firstName,
         lastName,
         email,
         mobile,
         profileImg,
         userName,
         password,
      } = req?.body;

      const user = await getUserByIdV2(req.params.id);
      if (!user) {
         return res.status(404).json({ message: 'User not found' });
      }

      const data = {
         firstName: firstName || user.firstName,
         lastName: lastName || user.lastName,
         email: email || user.email,
         mobile: mobile || user.mobile,
         profileImg: profileImg || user.profileImg,
         userName: userName || user.userName,
         password: password || user.password,
      };
      const updatedUser: any = await updateUser(req.params.id, data);
      res.json(updatedUser);
   } catch (error) {
      next(error);
   }
};

export const deleteById = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const users = await deleteUser(req.params.id);
      if (!users) {
         return res.status(404).json({ message: 'Users not found' });
      }
      res.json({ message: 'Users deleted' });
   } catch (error) {
      next(error);
   }
};

export const loginUser = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const userName = req.body.userName;
      const inputPassword = req.body.password;
      let user;
      if (!userName) {
         return res.status(400).json({ message: 'Username is required.', status: 'success' });
      } else {
         user = await getUserByUsername(userName);
      }
      if (user) {
         const hashedPassword = user.password;
         const isMatch = inputPassword == hashedPassword ? true : false;
         if (isMatch) {
            return res.status(200).json(user);
         } else {
            return res.status(400).json({ message: 'Invalid password.', status: 'success' });
         }
      } else {
         return res.status(400).json({ message: 'User does not exist.', status: 'success' });
      }
   } catch (error) {
      next(error);
   }
};