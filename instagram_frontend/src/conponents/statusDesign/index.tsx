import styles from './styles.module.css';
import { Text } from '..';

const StatusUI = (props: any) => {
  const {
    label = '',
    style,
    src = "",
    width = 50,
    height = 50,
    className,
  } = props;

  return (
    <div className='text-center'>
      <div className={`${styles.status_wrapper} d-center rounded-circle`} >
        <div className={`${styles.status_image_wrapper} d-center rounded-circle`}>
          <img className={` rounded-circle`} src={src ? src : "/assets/camera.webp"} width={width} height={height} alt='image' />
        </div>
      </div>
      {
        label &&
        <Text size={10} fontWeight={400}>_abhi</Text>
      }
    </div>
  );
};

export default StatusUI;