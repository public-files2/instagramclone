// controllers/messages.controller.js
import { NextFunction, Request, Response } from 'express';
import {
   createMessage,
   getAllMessages,
   getMessageById,
   updateMessage,
   deleteMessage,
   getMessageByRoom,
} from '../services/message.services';

export const createMessageController = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const { message = '', sender = '', reciever = '', room = '', sent = false, createdtime = '' } = req?.body;

      const savedMessage = await createMessage({ message, sender });
      res.status(201).json({ msg: 'Message Created', data: savedMessage });
   } catch (error) {
      next(error);
   }
};

export const getAllMessagesController = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const { room, receiver, sender } = req.query;
      const messages = await getAllMessages(room, receiver, sender);
      res.json(messages);
   } catch (error) {
      next(error);
   }
};

export const updateMessageController = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const { message, sender } = req?.body;

      const messages = await getMessageById(req.params.id);
      if (!messages) {
         return res.status(404).json({ message: 'Message not found' });
      }

      const data = {
         message: message || message.message,
         sender: sender || message.sender,
      };
      const updatedMessage: any = await updateMessage(req.params.id, data);
      res.json(updatedMessage);
   } catch (error) {
      next(error);
   }
};

export const deleteMessageController = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const message = await deleteMessage(req.params.id);
      if (!message) {
         return res.status(404).json({ message: 'Message not found' });
      }
      res.json({ message: 'Message deleted' });
   } catch (error) {
      next(error);
   }
};
