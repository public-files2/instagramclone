
import { create_user, login_user } from "@/api/services/user";
import { AuthContext } from "@/context/user";
import { useMutation, useQuery } from "@tanstack/react-query";
import { useRouter } from "next/router";
import { useContext, useState } from "react";
import { toast } from "react-toastify";
import { useForm } from "../utils/hooks/useForm";

const useLoginHook = () => {
    const router = useRouter()
    const [screenType, setScreenType] = useState("login");
    const { loggedInUser, setLoggedInUser } = useContext(AuthContext);
    const { form, setForm, handleChange: handlechange } = useForm();
    const [otp, setOtp] = useState('')
    const LoginMutation = useMutation({
        mutationFn: (payload: any) => login_user(payload),
        onMutate: (variables: any) => {
            return {};
        },
        onError: (error: any) => {
            toast.success(error?.message)
        },
        onSuccess: async (res: any) => {
            console.log(res)
            if (res) {
                setLoggedInUser(res);
                localStorage.setItem('LOGGED_USER', JSON.stringify(res));
                router.push('/home')
                toast.success("Login successfull")
                clearForm()
            } else {
                toast.success("Something went wrong")
            }
        },
    });
    const onLogin = () => {
        if (!form?.userName || !form?.password) {
            toast.warning("Fill your credentials")
        } else {
            LoginMutation.mutate(form);
        }
    }
    const addUser = useMutation({
        mutationFn: (payload: any) => create_user(payload),
        onMutate: (variables: any) => {
            return {};
        },
        onError: (error: any) => {
            toast.error(error?.msg)
        },
        onSuccess: (data: any) => {
            setScreenType('login')
            toast.success("User created")
            clearForm()
        },
    });

    const handleSignUp = () => {
        if (form?.userName && form?.password) {
            addUser.mutate(form);
        } else {
            toast.error("add username and password")
        }
    }

    const clearForm = () => {
        setForm({ firstName: "", lastName: "", email: "", userName: "", password: "" })
    }

    return {
        screenType,
        setScreenType,
        handlechange,
        handleSignUp,
        onLogin,
        form,
        setForm,
    }
}
export default useLoginHook