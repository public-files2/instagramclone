import React, { useState, useContext, useEffect } from 'react';
import io from 'socket.io-client';
import ChatRoom from './chatroom';
import styles from './styles.module.css';
import { AuthContext } from '@/context/user';
import useMessanger from './hooks';
import { Col, Row } from 'react-bootstrap';
import { CustomButton, StatusUI, Text } from '@/conponents';
import useLogout from '../utils/hooks/logout';
import { MdOutlineLogout } from 'react-icons/md';
import VideoCallComponent from './videoCall';

const WebSocketComponent2 = () => {
  const newSocket: any = io('http://localhost:5000');
  const { loggedInUser } = useContext(AuthContext);
  const [isVideoCallActive, setIsVideoCallActive] = useState(false)
  const {
    allUsers,
    selectedUser,
    setSelectedUser,
    allMessages,
    isFetching,
  } = useMessanger()
  const [messages, setMessages] = useState<any>([]);
  const [screenType, setScreenType] = useState('');
  const [socket, setSocket] = useState<any>(newSocket);
  const { logOut } = useLogout()
  const connectAllmessages = () => {
    setMessages(allMessages)
  }
  useEffect(() => {
    setMessages(allMessages)
  }, [isFetching])
  const joinRoom = async (data: any) => {
    const sendingBy = {
      room: data?._id,
      username: data?.firstName,
      sender: loggedInUser?._id,
      receiver: data?._id,
    }
    setSelectedUser({ ...data, room: sendingBy?.room, username: sendingBy?.username })
    // const privateRoom = `${socket?.id}-${room}`;
    if (sendingBy?.room !== '' && sendingBy?.username !== '') {
      socket.emit('join_room', sendingBy);
      setSocket(socket);
      setScreenType('chat');
    }
    connectAllmessages()
  };

  return (
    <>
      <div className='d-center'>
        <h1>Socket IO</h1>
        <CustomButton sizeVariant="CUSTOM" className="rounded-circle" label={<MdOutlineLogout />} onClick={() => logOut()} />
      </div>

      <div className={styles.container}>
        <div>
          <div className='text-center'>
            <Text size={20} color="blue">{loggedInUser?.firstName}</Text>
          </div>
          <Row>
            <Col sm={3}>
              {
                allUsers?.data?.map((item: any, index: number) => {
                  return (
                    <>
                      {item?._id != loggedInUser?._id &&
                        <div key={index} role='button' onClick={() => joinRoom(item)} className="mt-2">
                          <div className="d-flex align-items-center gap-1">
                            <StatusUI src={item?.url} width={25} height={25} />
                            <div className="d-flex flex-column">
                              <Text size={11} textTransform="">
                                {item?.firstName} {item?.lastName}
                              </Text>
                              <Text size={10} textTransform="">
                                {item?.userName}
                              </Text>
                            </div>
                          </div>
                        </div>
                      }
                    </>
                  )
                })
              }
            </Col>
            <Col sm={9}>
              <div className={styles.chatContainer}>
                {screenType === 'chat' ?
                  <>
                    {
                      isFetching ?
                        <div className='d-center h-100'>
                          <Text size={20}>Loading.....</Text>
                        </div>
                        :
                        <ChatRoom
                          messages={messages}
                          setMessages={setMessages}
                          isVideoCallActive={isVideoCallActive}
                          socket={socket}
                          selectedUser={selectedUser}
                          setIsVideoCallActive={setIsVideoCallActive}
                        />
                    }
                  </>
                  :
                  <>
                    <Text size={20}>
                      Select a user to start chat...
                    </Text>
                  </>
                }
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </>
  );
};

export default WebSocketComponent2;