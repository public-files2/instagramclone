const apiEndPoint = {
   users: '/users',
   userById: (id: any) => `/users/${id}`,
   login: '/users/login',
   getPosts: '/posts',
   postById: (id: any) => `/posts/${id}`,
   likePostById: (id: any) => `/posts/like/${id}`,
   unlikePostById: (id: any) => `/posts/unlike/${id}`,
   commentPostById: (id: any) => `/posts/comment/${id}`,
   imageFileUploader: (imgType: any) => `/fileuploader/img/?imgType=${imgType}`,
   messages: '/messages',
};
export default apiEndPoint;
