import { FaWpforms } from "react-icons/fa";
import { GrGallery } from "react-icons/gr";

export const inputTypesList = [
    "text",
    "password",
    "email",
    "number",
    "tel",
    "search",
    "url",
    "date",
    "time",
    "datetime-local",
    "month",
    "week",
    "color",
    "checkbox",
    "radio",
    "file",
    "range",
    "submit",
    "reset",
    "button",
];

export const projectsList = [
    {
        projectName: "Gallery",
        link: "/gallery",
        icon: <GrGallery />,
    },
    {
        projectName: "Forms",
        link: "/generatedForm",
        icon: <FaWpforms />,
    },
]